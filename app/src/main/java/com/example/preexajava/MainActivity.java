package com.example.preexajava;

import androidx.appcompat.app.AppCompatActivity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    private Button btnEntrar;
    private Button btnSalir;
    private EditText txtNombre;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        iniciarComponentes();

        btnEntrar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                entrar();
            }
        });

        btnSalir.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                salir();
            }
        });
    }

    private void iniciarComponentes() {
        btnEntrar = findViewById(R.id.entrarButton);
        btnSalir = findViewById(R.id.salirButton);
        txtNombre = findViewById(R.id.txtNombre);
    }

    private void entrar() {
        String strNombre;

        strNombre = txtNombre.getText().toString();
        if (txtNombre.getText().toString().equals("")) {
            Toast.makeText(getApplicationContext(), "Nombre no válido", Toast.LENGTH_LONG).show();
        } else {
            Bundle bundle = new Bundle();
            bundle.putString("Nombre", strNombre);

            Intent intent = new Intent(MainActivity.this, reciboActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        txtNombre.setText("");
    }

    private void salir() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("¿Deseas cerrar la app?");
        builder.setPositiveButton("Sí", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                finish();
            }
        });
        builder.setNegativeButton("No", null);
        builder.show();
    }


}