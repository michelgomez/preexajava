package com.example.preexajava;


public class ReciboNomina {
    private int numRecibo;
    private String nombre;
    private float horasTrabNormal;
    private float horasTrabExtras;
    private int puesto;
    private float impuestoPorc;

    public ReciboNomina(int numRecibo, String nombre, float horasTrabNormal, float horasTrabExtras, int puesto, float impuestoPorc) {
        this.numRecibo = numRecibo;
        this.nombre = nombre;
        this.horasTrabNormal = horasTrabNormal;
        this.horasTrabExtras = horasTrabExtras;
        this.puesto = puesto;
        this.impuestoPorc = impuestoPorc;
    }

    public int getNumRecibo() {
        return numRecibo;
    }

    public void setNumRecibo(int numRecibo) {
        this.numRecibo = numRecibo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public float getHorasTrabNormal() {
        return horasTrabNormal;
    }

    public void setHorasTrabNormal(float horasTrabNormal) {
        this.horasTrabNormal = horasTrabNormal;
    }

    public float getHorasTrabExtras() {
        return horasTrabExtras;
    }

    public void setHorasTrabExtras(float horasTrabExtras) {
        this.horasTrabExtras = horasTrabExtras;
    }

    public int getPuesto() {
        return puesto;
    }

    public void setPuesto(int puesto) {
        this.puesto = puesto;
    }

    public float getImpuestoPorc() {
        return impuestoPorc;
    }

    public void setImpuestoPorc(float impuestoPorc) {
        this.impuestoPorc = impuestoPorc;
    }

    public float calcularSubtotal() {
        float pagoBase = 200;
        float incrementoPago = 0;
        float pagoPorHora = 0;

        // Calcular el incremento del pago según el puesto
        if (puesto == 1) {
            incrementoPago = 0.2f;
        } else if (puesto == 2) {
            incrementoPago = 0.5f;
        } else if (puesto == 3) {
            incrementoPago = 1f;
        }

        // Calcular el incremento del pago base
        pagoBase += pagoBase * incrementoPago;

        // Calcular el pago por horas normales
        float pagoHorasNormales = pagoBase * horasTrabNormal;

        // Calcular el pago por horas extras
        if (incrementoPago > 0) {
            pagoPorHora = pagoBase;
        }
        float pagoHorasExtras = pagoPorHora * horasTrabExtras * 2;

        // Calcular el subtotal
        float subtotal = pagoHorasNormales + pagoHorasExtras;

        return subtotal;
    }

    public float calcularImpuesto(){
        float subtotal = calcularSubtotal();
        impuestoPorc = 0.16f;
        float impuesto = subtotal * impuestoPorc;
        return impuesto;
    }

    public float calcularTotal(){
        float subtotal = calcularSubtotal();
        float impuesto = calcularImpuesto();
        float total = subtotal - impuesto;
        return total;
    }
}