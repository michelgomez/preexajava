package com.example.preexajava;

import android.content.DialogInterface;
import android.os.Bundle;

import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;
import android.widget.Toast;


public class reciboActivity extends AppCompatActivity {

    private Button btnCalcular;
    private Button btnLimpiar;
    private Button btnRegresar;
    private EditText txtHorasTrabajadas;
    private EditText txtNombre;
    private EditText txtNumRecibo;
    private EditText txtHorasExtras;
    private TextView lblSubtotal;
    private TextView lblImpuesto;
    private TextView lblTotal;
    private RadioButton rdbAuxiliar;
    private RadioButton rdbAlbanil;
    private RadioButton rdbIngObra;

    private ReciboNomina recibo = new ReciboNomina(0,"", 0f,0f,0,0f);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_recibo);


        iniciarComponentes();
        Bundle bundle = getIntent().getExtras();
        txtNombre.setText(bundle.getString("Nombre"));

        rdbAuxiliar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Auxiliar();
            }
        });
        rdbAlbanil.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Albanil();
            }
        });
        rdbIngObra.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IngObra();
            }
        });
        btnRegresar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                regresar();
            }
        });
        btnLimpiar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                limpiar();
            }
        });
        btnCalcular.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                calcular();
            }
        });
    }

    private void iniciarComponentes() {
        btnCalcular = findViewById(R.id.btnCalcular);
        btnLimpiar = findViewById(R.id.btnLimpiar);
        btnRegresar = findViewById(R.id.btnRegresar);
        txtNumRecibo = findViewById(R.id.txtNumRecibo);
        txtHorasExtras = findViewById(R.id.txtHorasExtras);
        txtHorasTrabajadas = findViewById(R.id.txtHorasTrabajadas);
        lblSubtotal = findViewById(R.id.lblSubtotal);
        lblImpuesto = findViewById(R.id.lblImpuesto);
        lblTotal = findViewById(R.id.lblTotal);
        rdbAuxiliar = findViewById(R.id.rdbAuxiliar);
        rdbAlbanil = findViewById(R.id.rdbAlbanil);
        rdbIngObra = findViewById(R.id.rdbIngObra);
        txtNombre = findViewById(R.id.txtNombre);
    }

    public void Auxiliar(){
        recibo.setPuesto(1);
    }

    public void Albanil(){
        recibo.setPuesto(2);
    }

    public void IngObra(){
        recibo.setPuesto(3);
    }

    public void limpiar (){
        // Limpia los campos de entrada y los valores de lblSubtotal, lblImpuesto y lblTotal
        txtNumRecibo.setText("");
        txtHorasTrabajadas.setText("");
        txtHorasExtras.setText("");
        lblSubtotal.setText("");
        lblImpuesto.setText("");
        lblTotal.setText("");
        rdbAuxiliar.setChecked(false);
        rdbAlbanil.setChecked(false);
        rdbIngObra.setChecked(false);
    }

    private void regresar() {
        AlertDialog.Builder confirmar = new AlertDialog.Builder(this);
        confirmar.setTitle("Cálculo de nómina");
        confirmar.setMessage("¿Desea regresar?");
        confirmar.setPositiveButton("Confirmar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
                finish();
            }
        });
        confirmar.setNegativeButton("Cancelar", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int which) {
            }
        });
        confirmar.show();
    }

    private void calcular() {
        String numReciboStr = txtNumRecibo.getText().toString().trim();
        String horasTrabajadasStr = txtHorasTrabajadas.getText().toString().trim();
        String horasExtrasStr = txtHorasExtras.getText().toString().trim();

        if (!numReciboStr.isEmpty() && !horasTrabajadasStr.isEmpty() && !horasExtrasStr.isEmpty() && (rdbAuxiliar.isChecked() || rdbAlbanil.isChecked() || rdbIngObra.isChecked())) {
            int numRecibo = Integer.parseInt(numReciboStr);
            float horasTrabajadas = Float.parseFloat(horasTrabajadasStr);
            float horasExtras = Float.parseFloat(horasExtrasStr);

            recibo.setNumRecibo(numRecibo);
            recibo.setHorasTrabNormal(horasTrabajadas);
            recibo.setHorasTrabExtras(horasExtras);

            float subtotal = recibo.calcularSubtotal();
            float impuesto = recibo.calcularImpuesto();
            float total = recibo.calcularTotal();

            lblSubtotal.setText(String.valueOf(subtotal));
            lblImpuesto.setText(String.valueOf(impuesto));
            lblTotal.setText(String.valueOf(total));
        } else {
            Toast.makeText(this, "Ingrese todos los datos", Toast.LENGTH_SHORT).show();
        }
    }



}